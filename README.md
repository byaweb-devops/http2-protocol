# HTTP2 Protocol


## Installation


```shell
# Replace php mod by php-fpm config
sudo apt-get install php7.4-fpm
sudo a2enmod proxy_fcgi setenvif
sudo a2dismod php7.4
sudo a2enconf php7.4-fpm
sudo a2dismod mpm_prefork
sudo a2enmod mpm_event
sudo service apache2 restart
```

## Configure virtualhost (apache)

```
<VirtualHost *:80>
    ...
    <FilesMatch \.php$>
        SetHandler "proxy:unix:/var/run/php/php7.4-fpm.sock|fcgi://localhost/"
    </FilesMatch>
    ...
    Protocols h2 http/1.1
</VirtualHost>
```

## Configure SSL certificate

```
<VirtualHost *:443>
    ...
    SSLEngine                on
    SSLCertificateFile       /etc/ssl/certificate.crt
    SSLCertificateKeyFile    /etc/ssl/private/private.key
    SSLCertificateChainFile  /etc/ssl/ca_bundle.crt
    ...
    <FilesMatch \.php$>
        SetHandler "proxy:unix:/var/run/php/php7.4-fpm.sock|fcgi://localhost/"
    </FilesMatch>
    ...
    Protocols h2 http/1.1
</VirtualHost>
```
